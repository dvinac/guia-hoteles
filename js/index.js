$(function(){
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $("#modalContacto").on('show.bs.modal', function (e) {
    console.log('el modal se esta mostrando');
    $('#contactoBtn').removeClass('btn-primary');
    $('#contactoBtn').addClass('btn-rojo');

  });
  $("#modalContacto").on('shown.bs.modal', function (e) {
    console.log('el modal se mostró');
    $('#contactoBtn').prop('disabled', true);
  });
  $("#modalContacto").on('hide.bs.modal', function (e) {
    console.log('el modal se esta ocutando');
    $('#contactoBtn').prop('disabled', false);
  });
  $("#modalContacto").on('hidden.bs.modal', function (e) {
    console.log('el modal se ocultó');
    $('#contactoBtn').removeClass('btn-rojo');
    $('#contactoBtn').addClass('btn-primary');
  });
  $('.carousel').carousel({
      interval: 1000
  });
}); 
